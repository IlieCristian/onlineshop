package com.interview.onlineShop.config;

import com.interview.onlineShop.repository.ProductRepository;
import com.interview.onlineShop.service.ProductService;
import com.opencsv.CSVWriter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@Transactional
public class SchedulerConfig {

    private static final String STRING_ARRAY_SAMPLE = "./data/products.csv";
    private final ProductRepository productRepository;
    private final ProductService productService;

    public SchedulerConfig(ProductRepository productRepository, ProductService productService) {
        this.productRepository = productRepository;
        this.productService = productService;
    }

//    @Scheduled(fixedDelay = 5000)
    @Scheduled(cron = "0 0 0 * * *")
    public void scheduleFixedRateTask() {
        System.out.println("Fixed rate task - " + System.currentTimeMillis() / 1000);

        try (
                Writer writer = Files.newBufferedWriter(Paths.get(STRING_ARRAY_SAMPLE));
                CSVWriter csvWriter = new CSVWriter(writer,
                        CSVWriter.DEFAULT_SEPARATOR,
                        CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
        ) {
            String[] headerRecord = {"Id", "Name", "Description", "Latest Price"};
            csvWriter.writeNext(headerRecord);

            productRepository.findAll()
                    .stream()
                    .forEach(product -> {
                        boolean isProductValid = productService.isProductValid(product);
                        if (isProductValid) {

                            final String latestPrice = productService.getLatestPriceOf(product);
                            csvWriter.writeNext(new String[]{
                                    String.valueOf(product.getId()),
                                    product.getName(),
                                    product.getDescription(),
                                    latestPrice
                            });
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
