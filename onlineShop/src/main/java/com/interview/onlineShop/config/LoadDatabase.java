package com.interview.onlineShop.config;

import com.interview.onlineShop.constants.GlobalConstants;
import com.interview.onlineShop.model.Currency;
import com.interview.onlineShop.model.Flag;
import com.interview.onlineShop.model.PriceHistory;
import com.interview.onlineShop.model.Product;
import com.interview.onlineShop.model.enumeration.CurrencyType;
import com.interview.onlineShop.model.enumeration.FlagType;
import com.interview.onlineShop.repository.CurrencyRepository;
import com.interview.onlineShop.repository.FlagRepository;
import com.interview.onlineShop.repository.PriceHistoryRepository;
import com.interview.onlineShop.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Configuration
public class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);
    private static final Random random = new Random();

    private ZonedDateTime getZonedDateTime(String dateString) {
        ZoneId timeZone = ZoneId.systemDefault();
        ZonedDateTime zonedDateTime = LocalDateTime.parse(dateString,
                DateTimeFormatter.ISO_DATE_TIME).atZone(timeZone);
        log.info(zonedDateTime.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));

        return zonedDateTime;
    }

    private ZonedDateTime getRandomZonedDateTime() {
        String month = Integer.toString(1 + this.random.nextInt(12));
        String day = Integer.toString(1 + this.random.nextInt(28));

        if (month.length() < 2)
            month = "0" + month;
        if (day.length() < 2)
            day = "0" + day;

        return this.getZonedDateTime("2021-" + month + "-" + day + "T00:00:00");
    }

    private Product generateRandomProduct(String productCode, Flag invalidFlag) {
        StringBuilder name = new StringBuilder();
        StringBuilder description = new StringBuilder();
        final int NAME_LENGTH = 3;
        final int DESC_LENGTH = 10;

        final int UPPER_CASE_ALPHABET_LENGTH = GlobalConstants.UPPER_CASE_ALPHABET.length();
        for (int i = 0; i < NAME_LENGTH; i++) {
            name.append(GlobalConstants.UPPER_CASE_ALPHABET.charAt(random.nextInt(UPPER_CASE_ALPHABET_LENGTH)));
        }

        final int LOWER_CASE_ALPHABET_LENGTH = GlobalConstants.LOWER_CASE_ALPHABET.length();
        for (int i = 0; i < DESC_LENGTH; i++) {
            description.append(GlobalConstants.LOWER_CASE_ALPHABET.charAt(random.nextInt(LOWER_CASE_ALPHABET_LENGTH)));
        }

        Product product = new Product(productCode, name.toString(), description.toString());
        if (random.nextInt(10) > 7) {
            List<Flag> flags = new ArrayList<>();
            flags.add(invalidFlag);
            product.setFlags(flags);
        }
        return product;
    }

    private CommandLineRunner generateDatabase(ProductRepository productRepository,
                                               CurrencyRepository currencyRepository,
                                               PriceHistoryRepository priceHistoryRepository,
                                               FlagRepository flagRepository) {
        /* Currency List */
        List<Currency> currencies = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        List<PriceHistory> priceHistories = new ArrayList<>();
        List<Flag> flags = new ArrayList<>();

        /* Add Concurency Types */
        for (CurrencyType cType : CurrencyType.values()) {
            currencies.add(new Currency(cType, cType.getValue()));
        }

        /* Add Flag Types */
        for (FlagType flagType : FlagType.values()) {
            flags.add(new Flag(flagType));
        }

        final int MAX_MONEY_BOUND = 100;
        final int UPPER_CASE_ALPHABET_LENGTH = GlobalConstants.UPPER_CASE_ALPHABET.length();
        int currentProductCode = 0;

        /* Generate Products and their own PriceHistory data */
        for (int i = 0; i < GlobalConstants.GENERATED_DB_PRODUCT_NR; ++i) {
            char productCodeDigit = GlobalConstants.UPPER_CASE_ALPHABET.charAt(random.nextInt(UPPER_CASE_ALPHABET_LENGTH));
            final Flag invalidFlag = flags.get(0);
            Product newProd = generateRandomProduct(productCodeDigit + Integer.toString(currentProductCode++),
                    invalidFlag);

            for (int j = 0; j < GlobalConstants.GENERATED_DB_PRICE_PER_PRODUCT; ++j) {
                PriceHistory priceHistory = new PriceHistory(
                        new BigDecimal(random.nextInt(GlobalConstants.GENERATED_DB_MAX_MONEY_BOUND)),
                        this.getRandomZonedDateTime());
                priceHistory.setProduct(newProd);
                priceHistory.setCurrency(currencies.get(random.nextInt(currencies.size())));
                priceHistories.add(priceHistory);
            }
            products.add(newProd);
        }

        return args -> {
            /* Create Flags */
            for (Flag flag : flags) {
                log.info("Preloading " + flagRepository.save(flag));
            }

            /* Create Currencies */
            for (Currency currency : currencies) {
                log.info("Preloading " + currencyRepository.save(currency));
            }

            /* Create Products */
            for (Product product : products) {
                log.info("Preloading " + productRepository.save(product));
            }

            /* Create Price Histories */
            for (PriceHistory priceHistory : priceHistories) {
                log.info("Preloading " + priceHistoryRepository.save(priceHistory));
            }
        };
    }

    @Bean
    CommandLineRunner initDatabase(ProductRepository productRepository,
                                   CurrencyRepository currencyRepository,
                                   PriceHistoryRepository priceHistoryRepository,
                                   FlagRepository flagRepository) {

        if (GlobalConstants.GENERATE_DB) {
            return generateDatabase(productRepository,
                    currencyRepository,
                    priceHistoryRepository,
                    flagRepository);
        }
        return args -> { };
    }
}
