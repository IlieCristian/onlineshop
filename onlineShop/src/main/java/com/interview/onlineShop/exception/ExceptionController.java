package com.interview.onlineShop.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(ProductNotFoundException.class)
    ResponseEntity<Object> productNotFoundHandler(ProductNotFoundException ex) {
        Response response = new Response(
                HttpStatus.NOT_FOUND, ex.getLocalizedMessage());
        return new ResponseEntity<Object>(
                response, new HttpHeaders(), response.getStatus());
    }

    @ExceptionHandler(ProductNotValidException.class)
    ResponseEntity<Object> productNotValidHandler(ProductNotValidException ex) {
        Response response = new Response(
                HttpStatus.UNAUTHORIZED, ex.getLocalizedMessage());
        return new ResponseEntity<Object>(
                response, new HttpHeaders(), response.getStatus());
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        Response response = new Response(
                HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage());
        return new ResponseEntity<Object>(
                response, new HttpHeaders(), response.getStatus());
    }

}
