package com.interview.onlineShop.exception;

import com.interview.onlineShop.util.LanguageHelper;

public class ProductNotValidException extends RuntimeException {

    public ProductNotValidException(Long id) {
        super(LanguageHelper.getTranslation("product.notValidById") + " " + id);
    }

    public ProductNotValidException(String productCode) {
        super(LanguageHelper.getTranslation("product.notValidByCode") + " " + productCode);
    }
}