package com.interview.onlineShop.exception;

import com.interview.onlineShop.util.LanguageHelper;

public class ProductNotFoundException extends RuntimeException {

    public ProductNotFoundException(Long id) {
        super(LanguageHelper.getTranslation("product.notFoundById") + " " + id);
    }

    public ProductNotFoundException(String productCode) {
        super(LanguageHelper.getTranslation("product.notFoundByCode") + " " + productCode);
    }
}