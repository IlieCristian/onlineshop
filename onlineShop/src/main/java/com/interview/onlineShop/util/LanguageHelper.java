package com.interview.onlineShop.util;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Locale;

public class LanguageHelper {

    private static ResourceBundleMessageSource messageSource;

    static {
        messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("lang/res");
    }

    public static String getTranslation(String property) {
        return messageSource.getMessage(property, null, LocaleContextHolder.getLocale());
    }

    public static Locale getLanguage() {
        return LocaleContextHolder.getLocale();
    }
}
