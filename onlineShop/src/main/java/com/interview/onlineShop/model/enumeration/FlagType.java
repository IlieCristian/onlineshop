package com.interview.onlineShop.model.enumeration;

public enum FlagType {
    INVALID("INVALID"),
    OUT_OF_STOCK("OUT_OF_STOCK"),
    PROMOTED("PROMOTED");

    private final String text;

    FlagType(final String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return this.getText();
    }
}
