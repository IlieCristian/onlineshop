package com.interview.onlineShop.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.interview.onlineShop.model.enumeration.CurrencyType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "currency")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Currency {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(name = "type", nullable = false)
    private CurrencyType type;

    @NotNull
    @Column(name = "value", nullable = false)
    private BigDecimal value;

    @OneToMany(mappedBy = "currency")
    @JsonIgnoreProperties("currency")
    private List<PriceHistory> priceHistoryList;

    public Currency() {}

    public Currency(@NotNull CurrencyType type, @NotNull BigDecimal value) {
        this.type = type;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public CurrencyType getType() {
        return type;
    }

    public void setType(CurrencyType type) {
        this.type = type;
    }

    public List<PriceHistory> getPriceHistoryList() {
        return priceHistoryList;
    }

    public void setPriceHistoryList(List<PriceHistory> priceHistoryList) {
        this.priceHistoryList = priceHistoryList;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "id=" + id +
                ", type=" + type.toString() +
                ", value=" + value +
                ", priceHistoryList=" + priceHistoryList +
                '}';
    }
}
