package com.interview.onlineShop.model.enumeration;

import com.interview.onlineShop.constants.GlobalConstants;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum CurrencyType {
    EUR("EUR", "1"),
    USD("USD", "0.82"),
    RON("RON", "0.21"),
    YEN("YEN", "0.0078");

    private final String text;
    private final BigDecimal value;

    CurrencyType(final String text, final String value) {
        this.text = text;
        this.value = new BigDecimal(value)
                .setScale(GlobalConstants.BIG_DECIMAL_PRECISION, RoundingMode.HALF_UP);
    }

    public String getText() {
        return text;
    }

    public BigDecimal getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getText();
    }
}
