package com.interview.onlineShop.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Entity
@Table(name = "price_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PriceHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(name = "price", nullable = false)
    private BigDecimal price;

    @NotNull
    @Column(name = "recorded_date", nullable = false)
    private ZonedDateTime recordedDate;

    @ManyToOne
    @JoinColumn(name="product_id")
    @JsonIgnoreProperties("priceHistoryList")
    private Product product;

    @ManyToOne
    @JoinColumn(name="currency_id")
    @JsonIgnoreProperties("priceHistoryList")
    private Currency currency;

    public PriceHistory() {};

    public PriceHistory(@NotNull BigDecimal price, @NotNull ZonedDateTime recordedDate) {
        this.price = price;
        this.recordedDate = recordedDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ZonedDateTime getRecordedDate() {
        return recordedDate;
    }

    public void setRecordedDate(ZonedDateTime recordedDate) {
        this.recordedDate = recordedDate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
