package com.interview.onlineShop.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.interview.onlineShop.model.enumeration.FlagType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "flag")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Flag {

    @Id
    @GeneratedValue()
    private Long id;

    @NotNull
    @Column(name = "type", nullable = false)
    private FlagType type;

    @ManyToMany(mappedBy = "flags")
    @JsonIgnoreProperties("flags")
    List<Product> products;

    public Flag() {

    }

    public Flag(@NotNull FlagType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FlagType getType() {
        return type;
    }

    public void setType(FlagType type) {
        this.type = type;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
