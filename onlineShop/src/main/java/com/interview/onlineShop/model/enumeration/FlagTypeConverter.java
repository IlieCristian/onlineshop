package com.interview.onlineShop.model.enumeration;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class FlagTypeConverter implements AttributeConverter<FlagType, String> {
    @Override
    public String convertToDatabaseColumn(FlagType flagType) {
        if (flagType == null) {
            return null;
        }
        return flagType.toString();
    }

    @Override
    public FlagType convertToEntityAttribute(String text) {
        if (text == null) {
            return null;
        }

        return Stream.of(FlagType.values())
                .filter(c -> c.toString().equals(text))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
