package com.interview.onlineShop.model.enumeration;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class CurrencyTypeConverter implements AttributeConverter<CurrencyType, String> {
    @Override
    public String convertToDatabaseColumn(CurrencyType currencyType) {
        if (currencyType == null) {
            return null;
        }
        return currencyType.toString();
    }

    @Override
    public CurrencyType convertToEntityAttribute(String text) {
        if (text == null) {
            return null;
        }

        return Stream.of(CurrencyType.values())
                .filter(c -> c.toString().equals(text))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
