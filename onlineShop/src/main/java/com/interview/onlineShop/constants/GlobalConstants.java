package com.interview.onlineShop.constants;

public class GlobalConstants {
    public static boolean GENERATE_DB = true;
    public static Integer GENERATED_DB_PRODUCT_NR = 100;
    public static Integer GENERATED_DB_PRICE_PER_PRODUCT = 3;
    public static Integer GENERATED_DB_MAX_MONEY_BOUND = 100;

    public static Integer BIG_DECIMAL_PRECISION = 5;

    public static final String UPPER_CASE_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String LOWER_CASE_ALPHABET = "abcdefghijklmnopqrstuvwxyz";

}
