package com.interview.onlineShop.service;

import com.interview.onlineShop.exception.ProductNotFoundException;
import com.interview.onlineShop.model.Flag;
import com.interview.onlineShop.model.PriceHistory;
import com.interview.onlineShop.model.Product;
import com.interview.onlineShop.model.enumeration.CurrencyType;
import com.interview.onlineShop.model.enumeration.FlagType;
import com.interview.onlineShop.repository.PriceHistoryRepository;
import com.interview.onlineShop.repository.ProductRepository;
import com.interview.onlineShop.util.LanguageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductService.class);
    private final ProductRepository productRepository;
    private final PriceHistoryRepository priceHistoryRepository;

    public ProductService(ProductRepository productRepository, PriceHistoryRepository priceHistoryRepository) {
        this.productRepository = productRepository;
        this.priceHistoryRepository = priceHistoryRepository;
    }

    private static String transformCurrency(PriceHistory priceHistory) {
        final BigDecimal price = priceHistory.getPrice();
        BigDecimal currencyValue;
        String priceCurrenctyType;

        switch (LanguageHelper.getLanguage().toString()) {
            default:
            case ("en"): {
                priceCurrenctyType = CurrencyType.EUR.toString();
                currencyValue = CurrencyType.EUR.getValue();
                break;
            }
            case ("ro"): {
                priceCurrenctyType = CurrencyType.RON.toString();
                currencyValue = CurrencyType.RON.getValue();
                break;
            }
        }
        return price.multiply(currencyValue)
                .setScale(4, RoundingMode.UNNECESSARY).toString()
                + " " + priceCurrenctyType;
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Product find(Long id) {
        return productRepository
                .findById(id)
                .orElseThrow(() -> new ProductNotFoundException(LanguageHelper.getTranslation("product.notFoundById")));
    }

    public Product findByProductCode(String productCode) {
        return productRepository
                .findByProductCode(productCode)
                .orElseThrow(() -> new ProductNotFoundException(productCode));
    }

    public boolean isProductValid(Product product) {
        /* Check if the state is valid */
        Optional<Flag> isValid = product.getFlags().stream()
                .filter(flag -> FlagType.INVALID.equals(flag.getType()))
                .findAny();

        return !isValid.isPresent();
    }

    public String getLatestPriceOf(Product product) {

        /* Get latest Price History */
        return product.getPriceHistoryList()
                .stream()
                .min(Comparator.comparing(PriceHistory::getRecordedDate,
                        Comparator.nullsLast(Comparator.reverseOrder())))
                .map(ProductService::transformCurrency)
                .get();
    }
}
