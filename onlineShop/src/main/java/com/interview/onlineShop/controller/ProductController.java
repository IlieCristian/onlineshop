package com.interview.onlineShop.controller;

import com.interview.onlineShop.exception.ProductNotValidException;
import com.interview.onlineShop.model.Product;
import com.interview.onlineShop.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping()
    public ResponseEntity<List<Product>> getAll() {
        List<Product> productList = productService.findAll();

        return new ResponseEntity<>(productList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Product>> getById(@PathVariable Long id) {
        Product product = productService.find(id);

        return new ResponseEntity<>(Optional.ofNullable(product), HttpStatus.OK);
    }

    @GetMapping("/productCode/{productCode}")
    public ResponseEntity<Optional<Product>> getByProductCode(@PathVariable String productCode) {
        final Product product = productService.findByProductCode(productCode);
        if (!productService.isProductValid(product))
            new ProductNotValidException(product.getProductCode());

        return new ResponseEntity<>(Optional.ofNullable(product), HttpStatus.OK);
    }

    @GetMapping("/productCode/latestPrice/{productCode}")
    public ResponseEntity<String> getLatestPriceByProductCode(@PathVariable String productCode) {
        final Product product = productService.findByProductCode(productCode);
        if (!productService.isProductValid(product))
            throw new ProductNotValidException(product.getProductCode());
        final String latestPrice = productService.getLatestPriceOf(product);

        return new ResponseEntity<>(latestPrice, HttpStatus.OK);
    }
}
